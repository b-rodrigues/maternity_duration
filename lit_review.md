# Papers on competing risks

## Short summaries

**McCall 1996**: unemployment

**Heckman and Honoré 1989** and **Crowder 2001**: identifiability of competing risks models

**Addison and Portugal, 2001**: *Unemployment duration: Competing and defective risks*

The authors examine the determinants of unemployment duration using a competing risks model with
two destination states, inactivity and employment. The major contribution of this paper is to 
also consider defective risks, meaning risks that will never be attained. For example, someone might
be indefinitely looking for a job, either because of unlucky draws or because the person rejects
the job offers. This means that the duration becomes infinite.They quarterly use data from the 
Portuguese labour market from 1992(2)-1997(4). The authors find that it is not unlucky draws that 
explain defective risks, but the fact that there are two sub-populations, one of movers and one of 
stayers and it is the latter that produce defective risks. They also show that unemployment
benefits have the negative effect of slowing transitions out of unemployment and increasing
the proportion of long-term unemployment. Older workers also tend to stay permanently unemployed.

**Mussida, 2007**: *Unemployment duration and competing risks: a regional investigation*

This paper focuses on long term unemployment in Italy using data from a survey conducted in 2004
and 2005. Interesting presentation of the Kaplan-Meier estimator as well as a non-parametric 
estimation of the hazard function. A summary of  duration models is also presented. 

They use BASiD data, which are linked administrative records from the German statutory pension 
insurance scheme and the Federal Employment Agency. They comprise a 1% random sample of pension
account holders which are around 579K individuals. They use a very similar sample as in the 
Wilke 2014 paper, restricted to females aged 18-45 who give birth to their first child in the
period 1986-2005 and who were full-time employed at the time of conception. The sample comprises
19537 maternity durations.

**Deng et al.**: mortgage payments

**Fine and Gray 1999** *A proportional Hazards Model for the Subdistribution of a Competing Risk*





**Wilke 2014:** *The sorting of female careers after first birth: A competing risks analysis of
maternity leave duration*

Wilke et al. estimate a competing risks model where a mother chooses among the following 
alternatives:

1. returning to the same employer full-time $U_{FT, t}$
2. returning to the same employer part-time $U_{PT, t}$
3. entering a new job with a new employer $U_{NJ, t}$
4. registering unemployed and searching for a new job $U_{U, t}$
5. heaving a next child $U_{NC, t}$
6. entering an other state (education, training, self-employment, minor employment) $U_{oth, t}$

The authors want to test the following hypotheses:

1. H1 Transitions should mainly occur at the kinks of $U_{H,t}$ (utility of staying home and taking 
care of a child full-time)
2. H2 The higher a woman’s productivity and the better the availability of childcare, the more likely
a woman returns to (fulltime) employment early after birth.
3. H3 The shorter the job protection period and the better the pre-birth job match, the more likely 
women return to their previous employer.
4. H4 The more a pre-birth firm offers family-friendly job schemes, the more likely a mother returns 
to her previous employer, albeit more often in part-time.
5. H5 Transitions to registered unemployment mainly occur at the upward kinks of $U_{U,t}$ during 
periods of favorable labour market conditions. Low-productivity women in low-income households are 
more likely to enter unemployment.
6. H6 The higher a woman’s productivity and the shorter the job protection period, the less likely
she will have a next child before returning to employment.

Table 2 from their paper shows which variable they use to proxy for productivity and other
characteristics:

* Individual productivity: wage (by quantile within year), total labour market experience (in 
months), past unemployment experiences (yes/no)
* Pre-birth job characteristics and match quality: tenure (in months), increase/decrease in pre-birth
wage quintile, complexity of occupation (3 categories), occupational risk of shift work/overtime
* Pre-birth firm characteristics: firm size (3 dummies), share of female pre-employer staff, share
of pre-employer staff aged < 30
* Availability of child-care: child-care places per child aged < 3 (annual state level data)
* Leave legislation: job protection period (4 dummies with < 10, 10 - 12, 15 - 18, 36 months),
maximum maternity benefit entitlements (in Euro, deflated in 1995 prices)
* Labour market conditions: GDP growth rate (national), unemployment rate (by education/year/state)
* Further control variables: age, inactivity-illness during pregnancy, federal state of residence
(dummies), decade dummies (1980s, 1990s)

Compared to Wilke et al.:

* We can test for the same hypotheses
* We mostly have the same covariates: some doubts on child-care availability and unemployment rate
by education
* For residence, we can have a dummy per region. But in the case of France, a dummy for "living inactivity 
Ile de France or not" should be enough.
* Risk of shit work/overtime can be computed using the "Conditions de travail" data set.
* We have the distance between the residence and the job.
* We can also add a dummy for "is currently or was married" (because we do not know if a woman has
divorced)
* Number of young children (less than 3 years old? than 6?) in the household
* Penalized splines?

The authors arrive at the following conclusions:

* the higher a woman’s pre-birth wage, the more likely she returns to employment
* low-productivity women are less likely to register unemployed 
* Moreover, mothers with past unemployment experiences are more likely to enter unemployment but
less likely to return to employment 
* individual characteristics do not seem to be strongly related to the probability of having a 
next child within one or three years after birth
* tenure as an indicator of a good pre-birth job match increases the probability of returning to
the previous employer and decreases the probability of an employer change 
* Mothers who have been recently demoted in the salary distribution are more likely to deliver 
their second child out of inactivity and are less likely to immediately return the previous 
employer
* Mothers previously working with small firms are less likely to return and more likely to end
up in registered unemployment, while mothers in large firms are more likely to return to the 
previous employer, albeit more so in part-time.
* Mothers working in firms with a larger share of females are more likely to deliver their second
child and change employer but less likely to return full-time.
* A pre-birth employer with a very young workforce appears to push mothers to seek alternatives as
they are more likely to deliver their second child, end up in unemployment or start working for a
new employer, while returning to the pre-birth employer in full-time decreases. 
* A better availability of childcare, and hence lower childcare costs, clearly increases the 
employment rate of mothers and reduces the share of women who register unemployed or have their
second child out of inactivity 
* in case of extensive job protection of 36 months, the share of women returning to the previous
workplace tends to decline while the share entering a new job tends to increase with the length
of job protection 
* A generous job protection strongly increases the share of women who deliver their 
second child out of inactivity within three years after the first birth
* Unfavourable labor market conditions as reflected in a high unemployment rate make women 
exercise their right to return to their previous employer rather than quitting the job 

**Dlugosz, Lo and Wilke (2016):** *Competing risks quantile regression at work: in-depth exploration
of the role of public child support for the duration of maternity leave*

* The authors wrote and use the `cmprskQR` package to estimate a model of competing risks. However
they only report the results for 2 of the 6 competing risks from Wilke 2014.

**Fitzenberger:** *Return-tojob during and after parental leave*

* Only uses data from a single firm (to exclude biases through across-firm heterogeneity, lol)
* Very short section on institutional background

The want to test the following hypotheses:

1. H1 The birth of the first child results in a substantial decline in employment after birth.
2. H2 A higher career orientation before birth is positively associated with employment after birth 
3. H3 Longer PL durations are associated with lower stability of employment after return to job
4. H4 Working part-time during PL is positively associated with employment after birth
5. H5 The positive association between career orientation of females and employment after birth is
stronger when working part-time during PL.

Fitzenberger et al.'s estimation strategy is different than Wilke et al. They estimate linear 
probability models from one state to the other and not a competing risks model. They arrive at the
following conclusions:

* high relative wage positions are associated with higher rates of return-to-job.
*  females with the lowest relative wage position exhibit similar return rates until 3.5 years
after birth, whereas females who are in the lower-medium part of the conditional wage distribution
are less likely to return
* females with frequent promotions return more quickly
* A supervisory role is associated with a higher return rate and clearly quicker returns.

## What do we take from all this?

* Defective risks, like in Addison and Portugal (2003)? A mother could not stay indefinitely in 
maternity leave, so this should not apply here. However competing risks models are presented in a 
very clear manner. We could take inspiration from their presentation.
