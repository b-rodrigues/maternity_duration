# README

This is the repository that holds the source code to our paper *The duration of maternity leave
in France and the transitions back to the labour market: results from a competing risks model*
