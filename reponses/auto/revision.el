(TeX-add-style-hook
 "revision"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("letter" "a4paper" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("babel" "french") ("floatrow" "capposition=top") ("geometry" "top=2.5cm" "bottom=2.5cm" "left=2.5cm" "right=2.5cm")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "letter"
    "letter12"
    "inputenc"
    "fontenc"
    "babel"
    "caption"
    "layout"
    "setspace"
    "soul"
    "ulem"
    "graphicx"
    "wrapfig"
    "lettrine"
    "multicol"
    "pstricks"
    "floatrow"
    "url"
    "calc"
    "tabularx"
    "multirow"
    "dcolumn"
    "float"
    "amsmath"
    "amssymb"
    "mathrsfs"
    "dsfont"
    "xcolor"
    "geometry")
   (LaTeX-add-labels
    "maternity_duration_by_transition"))
 :latex)

