(TeX-add-style-hook
 "mat_leave"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper" "12pt" "authoryear")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("caption" "tableposition=top") ("eurosym" "gen") ("threeparttable" "flushleft") ("subfig" "caption=false") ("geometry" "top=3cm" "bottom=3cm" "left=3cm" "right=3cm")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "tables/age_sample_1_birth"
    "tables/prop_transi_by_sub_group_fin"
    "tables/competing_risks_estimate"
    "tables/competing_risks_estimate_cluster_secteur"
    "article"
    "art12"
    "amsmath"
    "amssymb"
    "inputenc"
    "hyperref"
    "rotating"
    "tikz"
    "natbib"
    "caption"
    "booktabs"
    "eurosym"
    "graphicx"
    "float"
    "threeparttable"
    "dsfont"
    "subfig"
    "geometry")
   (LaTeX-add-labels
    "intro"
    "institutional"
    "data"
    "maternity_duration_all_sample"
    "maternity_duration_by_transition"
    "econometric"
    "cox_ph"
    "subdist_hazard_fun"
    "fine_gray"
    "fine_gray2"
    "km_estimate"
    "cuminc"
    "reg_results"
    "conclusion")
   (LaTeX-add-bibliographies
    "biblio"))
 :latex)

